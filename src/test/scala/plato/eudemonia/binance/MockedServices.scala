package plato.eudemonia.binance

import akka.actor.ActorSystem
import akka.stream.Materializer
import plato.eudemonia.binance.http.{HttpClient, REST, RESTImpl, WS, WSImpl}
import org.scalamock.scalatest.MockFactory
import plato.eudemonia.binance.http.{REST, WS}

import scala.concurrent.ExecutionContext

trait MockedServices extends MockFactory {

  lazy implicit val mockedSystem: ActorSystem = ActorSystem()
  lazy implicit val mockedEc: ExecutionContext = mockedSystem.dispatcher
  lazy implicit val mockedMaterializer = Materializer(mockedSystem)

  lazy val mockedHttpClient: HttpClient = mock[HttpClient]
  lazy val rest: REST = new RESTImpl(mockedHttpClient)
  lazy val ws: WS = new WSImpl(mockedHttpClient)

}
