package plato.eudemonia.binance

package object syntax {

  object all extends CanHandleSyntax with CanHandleBinanceSyntax
}