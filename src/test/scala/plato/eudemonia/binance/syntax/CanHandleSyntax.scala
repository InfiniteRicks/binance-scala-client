package plato.eudemonia.binance.syntax

import plato.eudemonia.binance.client.exception.BinanceException
import plato.eudemonia.binance.http.{BType, FutureResponse}
import org.scalamock.handlers.CallHandler

import scala.concurrent.Future

class CanHandleOps[R](val canHandle: CallHandler[Future[R]]){
  def returnsFromFuture(r: R) = canHandle.returns(Future.successful(r))
  def failsInFuture(th: Throwable) = canHandle.returns(Future.failed(th))
}


class CanHandleBinanceOps[R](val canHandle: CallHandler[FutureResponse[R]]){
  def returnsSuccess(r: R) = canHandle.returns(Future.successful(Right(r): BType[R]))
  def returnsFailure(th: BinanceException) = canHandle.returns(Future.successful(Left(th): BType[R]))
}

trait CanHandleSyntax {
  implicit def ops[R](canHandle: CallHandler[Future[R]]): CanHandleOps[R] = new CanHandleOps[R](canHandle)
}

trait CanHandleBinanceSyntax {
  implicit def binanceOps[R](canHandle: CallHandler[FutureResponse[R]]): CanHandleBinanceOps[R] = new CanHandleBinanceOps[R](canHandle)
}
