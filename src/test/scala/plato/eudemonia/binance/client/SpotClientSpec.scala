package plato.eudemonia.binance.client

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import cats.implicits.catsSyntaxOptionId
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.parser._
import plato.eudemonia.binance.MockedServices
import plato.eudemonia.binance.client.common.Interval.`1h`
import plato.eudemonia.binance.client.common.parameters.{DepthLimit, DepthParams, KLinesParams}
import plato.eudemonia.binance.client.common.response._
import plato.eudemonia.binance.client.common.{BinanceBalance, KLine, OrderSide, Price}
import plato.eudemonia.binance.client.spot.parameters.{SpotOrderCancelAllParams, SpotOrderCancelParams, SpotOrderCreateParams}
import plato.eudemonia.binance.client.spot.response._
import plato.eudemonia.binance.client.spot._
import plato.eudemonia.binance.http.{RESTImpl, WSImpl}
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import plato.eudemonia.binance.syntax.all._
import plato.eudemonia.binance.client.spot.SpotOrderStatus.{CANCELED, FILLED}
import plato.eudemonia.binance.client.spot.{SpotOrderStatus, SpotOrderType, SpotTimeInForce}
import plato.eudemonia.binance.client.spot.SpotOrderType.{LIMIT, LIMIT_MAKER, MARKET, STOP_LOSS_LIMIT, TAKE_PROFIT_LIMIT}
import plato.eudemonia.binance.client.spot.SpotTimeInForce.GTC

import java.time.Instant

class SpotClientSpec extends AnyFlatSpecLike with Matchers with ScalaFutures with MockFactory {

  override implicit val patienceConfig: PatienceConfig = PatienceConfig(timeout = Span(10, Seconds), interval = Span(150, Millis))

  "getExchangeInformation" should "succeed" in new SetUp {
    val responseBody = parse(
      """
        |{
        |   "timezone":"UTC",
        |   "serverTime":1621543436177,
        |   "rateLimits":[
        |      {
        |         "rateLimitType":"REQUEST_WEIGHT",
        |         "interval":"MINUTE",
        |         "intervalNum":1,
        |         "limit":1200
        |      },
        |      {
        |         "rateLimitType":"ORDERS",
        |         "interval":"SECOND",
        |         "intervalNum":10,
        |         "limit":50
        |      },
        |      {
        |         "rateLimitType":"ORDERS",
        |         "interval":"DAY",
        |         "intervalNum":1,
        |         "limit":160000
        |      }
        |   ],
        |   "exchangeFilters":[
        |      
        |   ],
        |   "symbols":[
        |      {
        |         "symbol":"BNBBUSD",
        |         "status":"TRADING",
        |         "baseAsset":"BNB",
        |         "baseAssetPrecision":8,
        |         "quoteAsset":"BUSD",
        |         "quotePrecision":8,
        |         "quoteAssetPrecision":8,
        |         "baseCommissionPrecision":8,
        |         "quoteCommissionPrecision":8,
        |         "orderTypes":[
        |            "LIMIT",
        |            "LIMIT_MAKER",
        |            "MARKET",
        |            "STOP_LOSS_LIMIT",
        |            "TAKE_PROFIT_LIMIT"
        |         ],
        |         "icebergAllowed":true,
        |         "ocoAllowed":true,
        |         "quoteOrderQtyMarketAllowed":true,
        |         "isSpotTradingAllowed":true,
        |         "isMarginTradingAllowed":false,
        |         "filters":[
        |            {
        |               "filterType":"PRICE_FILTER",
        |               "minPrice":"0.01000000",
        |               "maxPrice":"10000.00000000",
        |               "tickSize":"0.01000000"
        |            },
        |            {
        |               "filterType":"PERCENT_PRICE",
        |               "multiplierUp":"5",
        |               "multiplierDown":"0.2",
        |               "avgPriceMins":5
        |            },
        |            {
        |               "filterType":"LOT_SIZE",
        |               "minQty":"0.01000000",
        |               "maxQty":"9000.00000000",
        |               "stepSize":"0.01000000"
        |            },
        |            {
        |               "filterType":"MIN_NOTIONAL",
        |               "minNotional":"10.00000000",
        |               "applyToMarket":true,
        |               "avgPriceMins":5
        |            },
        |            {
        |               "filterType":"ICEBERG_PARTS",
        |               "limit":10
        |            },
        |            {
        |               "filterType":"MARKET_LOT_SIZE",
        |               "minQty":"0.00000000",
        |               "maxQty":"1000.00000000",
        |               "stepSize":"0.00000000"
        |            },
        |            {
        |               "filterType":"MAX_NUM_ORDERS",
        |               "maxNumOrders":200
        |            },
        |            {
        |               "filterType":"MAX_NUM_ALGO_ORDERS",
        |               "maxNumAlgoOrders":5
        |            }
        |         ],
        |         "permissions":[
        |            "SPOT"
        |         ]
        |      }
        |   ]
        | }
        """.stripMargin
    )

    val request = HttpRequest(method = HttpMethods.GET,  uri = "http://rest/exchangeInfo")

    val response = (for {
      entity <- Marshal(responseBody).to[ResponseEntity]
      response = HttpResponse(status = StatusCodes.OK, entity = entity)
    } yield response).futureValue

    val expectedRateLimits = Seq(
      RateLimit(
        rateLimitType = RateLimitType.REQUEST_WEIGHT,
        interval = RateLimitInterval.MINUTE,
        intervalNum = 1,
        limit = 1200
      ),
      RateLimit(
        rateLimitType = RateLimitType.ORDERS,
        interval = RateLimitInterval.SECOND,
        intervalNum = 10,
        limit = 50
      ),
      RateLimit(
        rateLimitType = RateLimitType.ORDERS,
        interval = RateLimitInterval.DAY,
        intervalNum = 1,
        limit = 160000
      )
    )

    val expectedOrderTypes = Seq(
      LIMIT,
      LIMIT_MAKER,
      MARKET,
      STOP_LOSS_LIMIT,
      TAKE_PROFIT_LIMIT
    )

    val expectedFilters = Seq(
      Filter.PRICE_FILTER(minPrice = 0.01000000, maxPrice = 10000.00000000, tickSize = 0.01000000),
      Filter.PERCENT_PRICE(multiplierUp = 5, multiplierDown = 0.2, avgPriceMins = 5),
      Filter.LOT_SIZE(minQty = 0.01000000, maxQty = 9000.00000000, stepSize = 0.01000000),
      Filter.MIN_NOTIONAL(minNotional = 10.00000000, applyToMarket = true, avgPriceMins = 5),
      Filter.ICEBERG_PARTS(limit = 10),
      Filter.MARKET_LOT_SIZE(minQty = 0.00000000, maxQty = 1000.00000000, stepSize = 0.00000000),
      Filter.MAX_NUM_ORDERS(maxNumOrders = 200),
      Filter.MAX_NUM_ALGO_ORDERS(maxNumAlgoOrders = 5)
    )

    val expectedSymbols = Seq(
      Symbol(
        symbol = "BNBBUSD",
        status = "TRADING",
        baseAsset = "BNB",
        baseAssetPrecision = 8,
        quoteAsset = "BUSD",
        quotePrecision = 8,
        quoteAssetPrecision = 8,
        baseCommissionPrecision = 8,
        quoteCommissionPrecision = 8,
        orderTypes = expectedOrderTypes,
        icebergAllowed = true,
        ocoAllowed = true,
        quoteOrderQtyMarketAllowed = true,
        isSpotTradingAllowed = true,
        isMarginTradingAllowed = false,
        filters = expectedFilters,
        permissions = Seq("SPOT")
      )
    )

    val expectedResult = ExchangeInformation(
      timezone = "UTC",
      serverTime = 1621543436177L,
      rateLimits = expectedRateLimits,
      exchangeFilters = Seq.empty[Filter],
      symbols = expectedSymbols
    )

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.getExchangeInfo()){ infoOpt =>
      val info = infoOpt.toOption.get
      info.timezone should be(expectedResult.timezone)
      info.serverTime should be(expectedResult.serverTime)
      info.rateLimits.toSet should be(expectedResult.rateLimits.toSet)
      info.exchangeFilters.isEmpty should be(true)
      info.symbols.toSet should be(expectedSymbols.toSet)
    }
  }

  "getDepth" should "succeed" in new SetUp {

    val params = DepthParams("SYMBOL", DepthLimit.`100`)

    val responseBody = parse(
      """
        |{
        |  "lastUpdateId": 1027024,
        |  "bids": [
        |    ["4.00000000","431.00000000"]
        |  ],
        |  "asks": [
        |    ["4.00000200","12.00000000"]
        |  ]
        |}
        |""".stripMargin
    )

    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = "http://rest/depth?symbol=SYMBOL&limit=100"
    )

    val response =
      (for {
        entity <- Marshal(responseBody).to[RequestEntity]
        response = HttpResponse(status = StatusCodes.OK, entity = entity)
      } yield response).futureValue

    val expectedDepth = Depth(
      lastUpdateId = 1027024,
      asks = Seq(Ask(price = 4.00000200, quantity = 12.00000000)),
      bids = Seq(Bid(price = 4.00000000, quantity = 431.00000000))
    )

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.getDepth(params)) { depth =>
      depth.toOption should contain(expectedDepth)
    }
  }

  "getKLines" should "succeed" in new SetUp {

    val startTime = Instant.ofEpochMilli(1500000000000L)
    val endTime   = Instant.ofEpochMilli(1600000000000L)

    val params = KLinesParams("SYMBOL", `1h`, startTime = startTime, endTime = endTime, limit = 14)

    val responseBody = parse(
      """
        |[
        |  [1548836400000, "105.13000000", "105.14000000", "105.05000000", "105.09000000", "70.72517000", 1548836459999, "7432.93828700", 45, "36.68194000", "3855.32695710", "0"],
        |  [1548866279000, "108.39000000", "108.39000000", "108.15000000", "108.22000000", "327.08359000", 1548866339999, "35415.40478090", 129, "163.42355000", "17699.38253540", "0"]
        |]
              """.stripMargin
    )

    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = "http://rest/kLines?symbol=SYMBOL&interval=1h&startTime=1500000000000&endTime=1600000000000&limit=14"
    )

    val response =
      (for {
        entity <- Marshal(responseBody).to[RequestEntity]
        response = HttpResponse(status = StatusCodes.OK, entity = entity)
      } yield response).futureValue

    val expectedKines = Set(
      KLine(
        openTime = 1548836400000L,
        open = 105.13000000,
        high = 105.14000000,
        low = 105.05000000,
        close = 105.09000000,
        volume = 70.72517000,
        closeTime = 1548836459999L,
        quoteAssetVolume = 7432.93828700,
        numberOfTrades = 45,
        takerBuyBaseAssetVolume = 36.68194000,
        takerBuyQuoteAssetVolume = 3855.32695710
      ),
      KLine(
        openTime = 1548866279000L,
        open = 108.39000000,
        high = 108.39000000,
        low = 108.15000000,
        close = 108.22000000,
        volume = 327.08359000,
        closeTime = 1548866339999L,
        quoteAssetVolume = 35415.40478090,
        numberOfTrades = 129,
        takerBuyBaseAssetVolume = 163.42355000,
        takerBuyQuoteAssetVolume = 17699.38253540
      )
    )

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.getKLines(params)) { klines =>
      klines.toOption.map(_.toSet) should contain(expectedKines)
    }
  }

  "getPrices" should "succeed" in new SetUp {

    val responseBody = parse(
      """
        |[
        |    {
        |        "symbol": "ETHBTC",
        |        "price": "0.03444300"
        |    },
        |    {
        |        "symbol": "LTCBTC",
        |        "price": "0.01493000"
        |    }
        |]
        |""".stripMargin
    )

    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = "http://rest/tickerPrice"
    )

    val response =
      (for {
        entity <- Marshal(responseBody).to[RequestEntity]
        response = HttpResponse(status = StatusCodes.OK, entity = entity)
      } yield response).futureValue

    val expectedPrices = Set(Price("ETHBTC", 0.03444300), Price("LTCBTC", 0.01493000))

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.getPrices()) { prices =>
      prices.toOption.map(_.toSet) should contain(expectedPrices)
    }
  }

  "getBalance" should "succeed" in new SetUp {

    val fixedTime          = 1499827319559L
    val timestamp: Instant = Instant.ofEpochMilli(fixedTime)
    val signature          = "6cd35332399b004466463b9ad65a112a14f31fb9ddfd5e19bd7298fbd491dbc7"

    val responseBody = parse(
      """
        |{
        |  "makerCommission": 15,
        |  "takerCommission": 16,
        |  "buyerCommission": 0,
        |  "sellerCommission": 1,
        |  "canTrade": true,
        |  "canWithdraw": false,
        |  "canDeposit": true,
        |  "accountType": "SPOT",
        |  "permissions": [
        |    "SPOT"
        |  ],
        |  "updateTime": 123456789,
        |  "balances": [
        |    {
        |      "asset": "BTC",
        |      "free": "4723846.89208129",
        |      "locked": "0.00000000"
        |    },
        |    {
        |      "asset": "LTC",
        |      "free": "4763368.68006011",
        |      "locked": "0.00000001"
        |    }
        |  ]
        |}
        """.stripMargin
    )

    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = s"http://rest/account?timestamp=1499827319559&recvWindow=5000&signature=$signature",
      headers = Seq(RawHeader("X-MBX-APIKEY", apiKey))
    )

    val response =
      (for {
        entity <- Marshal(responseBody).to[RequestEntity]
        response = HttpResponse(status = StatusCodes.OK, entity = entity)
      } yield response).futureValue

    val expectedBalances = Set(
      BinanceBalance(asset = "BTC", free = 4723846.89208129, locked = 0.00000000),
      BinanceBalance(asset = "LTC", free = 4763368.68006011, locked = 0.00000001)
    )

    val expectedAccountInfo =
      SpotAccountInfoResponse(
        balances = expectedBalances.toSeq,
        makerCommission = 15,
        takerCommission = 16,
        buyerCommission = 0,
        sellerCommission = 1,
        canTrade = true,
        canWithdraw = false,
        canDeposit = true,
        accountType = "SPOT",
        permissions = List("SPOT"),
        updateTime = 123456789L
      )

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.getBalance(timestamp)) { accountInfoEither =>
      val accountInfoOption = accountInfoEither.toOption
      accountInfoOption.map(_.balances.toSet) should contain(expectedBalances)
      accountInfoOption.map(_.makerCommission) should contain(expectedAccountInfo.makerCommission)
      accountInfoOption.map(_.takerCommission) should contain(expectedAccountInfo.takerCommission)
      accountInfoOption.map(_.buyerCommission) should contain(expectedAccountInfo.buyerCommission)
      accountInfoOption.map(_.sellerCommission) should contain(expectedAccountInfo.sellerCommission)
      accountInfoOption.map(_.canTrade) should contain(expectedAccountInfo.canTrade)
      accountInfoOption.map(_.canWithdraw) should contain(expectedAccountInfo.canWithdraw)
      accountInfoOption.map(_.canDeposit) should contain(expectedAccountInfo.canDeposit)
      accountInfoOption.map(_.accountType) should contain(expectedAccountInfo.accountType)
      accountInfoOption.map(_.permissions) should contain(expectedAccountInfo.permissions)
      accountInfoOption.map(_.updateTime) should contain(expectedAccountInfo.updateTime)
    }
  }

  "createOrder" should "succeed" in new SetUp {

    val fixedTime          = 1499827319559L
    val timestamp: Instant = Instant.ofEpochMilli(fixedTime)
    val signature          = "19409c8a8ad1f699be6a9f548d4197e7b86c2013f1caa2dfd09addc7f13f2336"

    val params = SpotOrderCreateParams.MARKET(
      symbol = "BTCUSDT",
      side = OrderSide.BUY,
      quantity = BigDecimal(10.5).some
    )

    val responseBody = parse(
      """
        |{
        |  "symbol": "BTCUSDT",
        |  "orderId": 28,
        |  "orderListId": -1,
        |  "clientOrderId": "6gCrw2kRUAF9CvJDGP16IP",
        |  "transactTime": 1507725176595,
        |  "price": "0.00000000",
        |  "origQty": "10.00000000",
        |  "executedQty": "10.00000000",
        |  "cummulativeQuoteQty": "10.00000000",
        |  "status": "FILLED",
        |  "timeInForce": "GTC",
        |  "type": "MARKET",
        |  "side": "SELL",
        |  "fills": [
        |    {
        |      "price": "4000.00000000",
        |      "qty": "1.00000000",
        |      "commission": "4.00000000",
        |      "commissionAsset": "USDT"
        |    }
        |  ]
        |}
                      """.stripMargin
    )

    val request = HttpRequest(
      method = HttpMethods.POST,
      uri =
        s"http://rest/order?symbol=BTCUSDT&side=BUY&quantity=10.5&type=MARKET&newOrderRespType=FULL&timestamp=1499827319559&recvWindow=5000&signature=$signature",
      headers = Seq(RawHeader("X-MBX-APIKEY", apiKey))
    )

    val response =
      (for {
        entity <- Marshal(responseBody).to[RequestEntity]
        response = HttpResponse(status = StatusCodes.OK, entity = entity)
      } yield response).futureValue

    val expectedFills = Seq(
      SpotFill(
        price = 4000.00000000,
        qty = 1.00000000,
        commission = 4.00000000,
        commissionAsset = "USDT"
      )
    )

    val expectedResponse =
      SpotOrderCreateResponse(
        symbol = "BTCUSDT",
        orderId = 28,
        orderListId = -1,
        clientOrderId = "6gCrw2kRUAF9CvJDGP16IP",
        transactTime = 1507725176595L,
        price = 0.00000000,
        origQty = 10.00000000,
        executedQty = 10.00000000,
        cummulativeQuoteQty = 10.00000000,
        status = FILLED,
        timeInForce = GTC,
        `type` = SpotOrderType.MARKET,
        side = OrderSide.SELL,
        fills = expectedFills
      )

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.createOrder(params, timestamp)) { creationResult =>
      creationResult.toOption should contain(expectedResponse)
    }
  }

  "cancelOrder" should "succeed" in new SetUp {

    val fixedTime          = 1499827319559L
    val timestamp: Instant = Instant.ofEpochMilli(fixedTime)
    val signature          = "96a96a6525d0992502e180d20b41ab249256c3eceaf39ca6ebc5783a7d379b22"

    val params = SpotOrderCancelParams(symbol = "BTCUSDT", origClientOrderId = "myOrder1")

    val responseBody = parse(
      """
        |{
        |  "symbol": "LTCBTC",
        |  "origClientOrderId": "myOrder1",
        |  "orderId": 4,
        |  "orderListId": -1,
        |  "clientOrderId": "cancelMyOrder1",
        |  "price": "2.00000000",
        |  "origQty": "1.00000000",
        |  "executedQty": "0.00000000",
        |  "cummulativeQuoteQty": "0.00000000",
        |  "status": "CANCELED",
        |  "timeInForce": "GTC",
        |  "type": "LIMIT",
        |  "side": "BUY"
        |}
        """.stripMargin
    )

    val request = HttpRequest(
      method = HttpMethods.DELETE,
      uri =
        s"http://rest/order?symbol=BTCUSDT&origClientOrderId=myOrder1&timestamp=1499827319559&recvWindow=5000&signature=$signature",
      headers = Seq(RawHeader("X-MBX-APIKEY", apiKey))
    )

    val response =
      (for {
        entity <- Marshal(responseBody).to[RequestEntity]
        response = HttpResponse(status = StatusCodes.OK, entity = entity)
      } yield response).futureValue

    val expectedResponse =
      SpotOrderCancelResponse(
        symbol = "LTCBTC",
        origClientOrderId = "myOrder1",
        orderId = 4,
        orderListId = -1,
        clientOrderId = "cancelMyOrder1",
        price = 2.00000000,
        origQty = 1.00000000,
        executedQty = 0.00000000,
        cummulativeQuoteQty = 0.00000000,
        status = CANCELED,
        timeInForce = SpotTimeInForce.GTC,
        `type` = SpotOrderType.LIMIT,
        side = OrderSide.BUY
      )

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.cancelOrder(params, timestamp)) { cancelResult =>
      cancelResult.toOption should contain(expectedResponse)
    }
  }

  "cancelOrders" should "succeed" in new SetUp {

    val fixedTime          = 1499827319559L
    val timestamp: Instant = Instant.ofEpochMilli(fixedTime)
    val signature          = "5634d2bdfb9b723e6df85c1551c13acb90b0836c218bc8a08c597eba3f1563e7"

    val params = SpotOrderCancelAllParams(symbol = "BTCUSDT")

    val responseBody = parse(
      """
        |[
        | {
        |  "symbol": "LTCBTC",
        |  "origClientOrderId": "myOrder1",
        |  "orderId": 4,
        |  "orderListId": -1,
        |  "clientOrderId": "cancelMyOrder1",
        |  "price": "2.00000000",
        |  "origQty": "1.00000000",
        |  "executedQty": "0.00000000",
        |  "cummulativeQuoteQty": "0.00000000",
        |  "status": "CANCELED",
        |  "timeInForce": "GTC",
        |  "type": "LIMIT",
        |  "side": "BUY"
        | }
        |]
        """.stripMargin
    )

    val request = HttpRequest(
      method = HttpMethods.DELETE,
      uri = s"http://rest/openOrders?symbol=BTCUSDT&timestamp=1499827319559&recvWindow=5000&signature=$signature",
      headers = Seq(RawHeader("X-MBX-APIKEY", apiKey))
    )

    val response =
      (for {
        entity <- Marshal(responseBody).to[RequestEntity]
        response = HttpResponse(status = StatusCodes.OK, entity = entity)
      } yield response).futureValue

    val expectedResponse = Seq(
      SpotOrderCancelResponse(
        symbol = "LTCBTC",
        origClientOrderId = "myOrder1",
        orderId = 4,
        orderListId = -1,
        clientOrderId = "cancelMyOrder1",
        price = 2.00000000,
        origQty = 1.00000000,
        executedQty = 0.00000000,
        cummulativeQuoteQty = 0.00000000,
        status = SpotOrderStatus.CANCELED,
        timeInForce = SpotTimeInForce.GTC,
        `type` = SpotOrderType.LIMIT,
        side = OrderSide.BUY
      )
    )

    (mockedHttpClient.singleRequest _).expects(request).returnsFromFuture(response)

    whenReady(spotClient.cancelAllOrders(params, timestamp)) { creationResult =>
      creationResult.toOption should contain(expectedResponse)
    }
  }

  trait SetUp extends MockedServices {

    val uris = Uris(
      exchangeInfo = Uri("http://rest/exchangeInfo"),
      depth = Uri("http://rest/depth"),
      kLines = Uri("http://rest/kLines"),
      tickerPrice = Uri("http://rest/tickerPrice"),
      account = Uri("http://rest/account"),
      order = Uri("http://rest/order"),
      openOrders = Uri("http://rest/openOrders")
    )

    val apiKey    = "vmPUZE6mv9SD5VNHk4HlWFsOr6aKE2zvsw0MuIgwCIPy6utIco14y7Ju91duEh8A"
    val apiSecret = "NhqPtmdSJYdKjVHjA7PZj4Mge3R5YNiP1e3UZjInClVN65XAbvqqM6A7H5fATj0j"

    val config = SpotConfig(
      restBaseUrl = Uri("http://rest"),
      wsBaseUrl = Uri("http://ws"),
      apiKey = apiKey,
      apiSecret = apiSecret,
      recvWindow = 5000,
      testnet = false,
      uris = uris
    )

    val restClient = new RESTImpl(mockedHttpClient)
    val wsClient   = new WSImpl(mockedHttpClient)

    val spotClient = new SpotClientImpl(
      config,
      restClient,
      wsClient
    )
  }
}
