package plato.eudemonia.binance.client.exception

import io.circe.generic.JsonCodec

@JsonCodec
case class BinanceException(code: Int, message: String)
