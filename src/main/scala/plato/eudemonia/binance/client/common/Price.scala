package plato.eudemonia.binance.client.common

import io.circe.generic.JsonCodec

@JsonCodec
case class Price(symbol: String, price: BigDecimal)
