package plato.eudemonia.binance.client.common

import io.circe.generic.JsonCodec

@JsonCodec
case class BinanceBalance(asset: String, free: BigDecimal, locked: BigDecimal)
