package plato.eudemonia.binance.client.common

import enumeratum.{CirceEnum, Enum, EnumEntry}

sealed trait OrderSide extends EnumEntry

object OrderSide extends Enum[OrderSide] with CirceEnum[OrderSide] {
  val values = findValues

  case object SELL extends OrderSide
  case object BUY  extends OrderSide
}