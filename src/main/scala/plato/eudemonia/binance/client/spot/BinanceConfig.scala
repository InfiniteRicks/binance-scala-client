/*
 * Copyright (c) 2021 Paolo Boni
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package plato.eudemonia.binance.client.spot

import akka.http.scaladsl.model.Uri
import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus._

case class SpotConfig(
    restBaseUrl: Uri,
    wsBaseUrl: Uri,
    apiKey: String,
    apiSecret: String,
    recvWindow: Int,
    testnet: Boolean,
    uris: Uris
) {

  val exchangeInfoUrl: Uri = uris.exchangeInfo
  val depthUri: Uri        = uris.depth
  val kLinesUri: Uri       = uris.kLines
  val tickerPriceUri: Uri  = uris.tickerPrice
  val accountUri: Uri      = uris.account
  val orderUri: Uri        = uris.order
  val openOrdersUri: Uri   = uris.openOrders

}

object SpotConfig {

  def fromConfig(homeConfig: Config): SpotConfig = {
    val restBaseUrl: Uri  = Uri(homeConfig.as[String]("rest-base-url"))
    val wsBaseUrl: Uri    = Uri(homeConfig.as[String]("ws-base-url"))
    val apiKey: String    = homeConfig.as[String]("key")
    val apiSecret: String = homeConfig.as[String]("secret")
    val recvWindow: Int   = homeConfig.as[Int]("recv-window")
    val testnet: Boolean  = homeConfig.as[Boolean]("testnet")
    val uris: Uris        = Uris.fromConfig(homeConfig.getConfig("uris"))
    SpotConfig(
      restBaseUrl = restBaseUrl,
      wsBaseUrl = wsBaseUrl,
      apiKey = apiKey,
      apiSecret = apiSecret,
      recvWindow = recvWindow,
      testnet = testnet,
      uris = uris
    )
  }
}

case class Uris(
    exchangeInfo: Uri,
    depth: Uri,
    kLines: Uri,
    tickerPrice: Uri,
    account: Uri,
    order: Uri,
    openOrders: Uri
)

object Uris {

  def fromConfig(homeConfig: Config): Uris = {
    val exchangeInfo: Uri = Uri(homeConfig.as[String]("exchangeInfo"))
    val depth: Uri        = Uri(homeConfig.as[String]("depth"))
    val kLines: Uri       = Uri(homeConfig.as[String]("kLines"))
    val tickerPrice: Uri  = Uri(homeConfig.as[String]("tickerPrice"))
    val account: Uri      = Uri(homeConfig.as[String]("account"))
    val order: Uri        = Uri(homeConfig.as[String]("order"))
    val openOrders: Uri   = Uri(homeConfig.as[String]("openOrders"))
    Uris(
      exchangeInfo = exchangeInfo,
      depth = depth,
      kLines = kLines,
      tickerPrice = tickerPrice,
      account = account,
      order = order,
      openOrders = openOrders
    )
  }
}
