package plato.eudemonia.binance.client.spot.response

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import plato.eudemonia.binance.client.spot.SpotOrderType
import plato.eudemonia.binance.client.spot.SpotOrderType

case class Symbol(
    symbol: String,
    status: String,
    baseAsset: String,
    baseAssetPrecision: Int,
    quoteAsset: String,
    quotePrecision: Int,
    quoteAssetPrecision: Int,
    baseCommissionPrecision: Int,
    quoteCommissionPrecision: Int,
    orderTypes: Seq[SpotOrderType],
    icebergAllowed: Boolean,
    ocoAllowed: Boolean,
    quoteOrderQtyMarketAllowed: Boolean,
    isSpotTradingAllowed: Boolean,
    isMarginTradingAllowed: Boolean,
    filters: Seq[Filter],
    permissions: Seq[String]
)

object Symbol {
  implicit val decoder: Decoder[Symbol] = deriveDecoder[Symbol]
}
