/*
 * Copyright (c) 2021 Paolo Boni
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package plato.eudemonia.binance.client.spot

import akka.stream.Materializer
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import plato.eudemonia.binance.client.common._
import plato.eudemonia.binance.client.common.parameters._
import plato.eudemonia.binance.client.common.response._
import plato.eudemonia.binance.client.spot.parameters._
import plato.eudemonia.binance.client.spot.response._
import plato.eudemonia.binance.encryption.MkSignedUri
import plato.eudemonia.binance.http.QueryParamsConverter._
import plato.eudemonia.binance.http._
import plato.eudemonia.binance.client.common.KLine
import plato.eudemonia.binance.client.spot.parameters.{SpotOrderCancelAllParams, SpotOrderCancelParams}
import plato.eudemonia.binance.client.spot.parameters.SpotOrderCreateResponseType.FULL
import plato.eudemonia.binance.http.{REST, WS}

import java.time.Instant
import scala.concurrent.ExecutionContext

trait SpotClient {

  def getExchangeInfo(): FutureResponse[ExchangeInformation]

  def getDepth(query: DepthParams): FutureResponse[Depth]

  def getKLines(query: KLinesParams): FutureResponse[Seq[KLine]]

  def getPrices(): FutureResponse[Seq[Price]]

  def getBalance(timestamp: Instant): FutureResponse[SpotAccountInfoResponse]

  def createOrder(orderCreate: SpotOrderCreateParams, timestamp: Instant): FutureResponse[SpotOrderCreateResponse]

  def cancelOrder(orderCancel: SpotOrderCancelParams, timestamp: Instant): FutureResponse[SpotOrderCancelResponse]

  def cancelAllOrders(
      orderCancel: SpotOrderCancelAllParams,
      timestamp: Instant
  ): FutureResponse[Seq[SpotOrderCancelResponse]]

}

class SpotClientImpl(
    config: SpotConfig,
    restClient: REST,
    wsClient: WS
)(implicit ec: ExecutionContext, mat: Materializer)
    extends SpotClient {

  private val mkSignedUri = new MkSignedUri(
    recvWindow = config.recvWindow,
    apiSecret = config.apiSecret
  )

  override def getExchangeInfo(): FutureResponse[ExchangeInformation] = {
    val uri = config.exchangeInfoUrl
    restClient.get[ExchangeInformation](uri)
  }

  override def getDepth(query: DepthParams): FutureResponse[Depth] = {
    val uri = config.depthUri.withQuery(query.toQueryParams)
    restClient.get[Depth](uri)
  }

  override def getKLines(query: KLinesParams): FutureResponse[Seq[KLine]] = {
    val uri = config.kLinesUri.withQuery(query.toQueryParams)
    restClient.get[Seq[KLine]](uri)
  }

  override def getPrices(): FutureResponse[Seq[Price]] =
    restClient.get[Seq[Price]](config.tickerPriceUri)

  override def getBalance(timestamp: Instant): FutureResponse[SpotAccountInfoResponse] = {
    val uri = mkSignedUri(config.accountUri, timestamp)
    restClient.get[SpotAccountInfoResponse](
      uri = uri,
      headers = Map("X-MBX-APIKEY" -> config.apiKey)
    )
  }

  override def createOrder(
      orderCreate: SpotOrderCreateParams,
      timestamp: Instant
  ): FutureResponse[SpotOrderCreateResponse] = {
    val params = orderCreate.toQueryParams.append("newOrderRespType", FULL.toString)
    val uri    = mkSignedUri(uri = config.orderUri, timestamp = timestamp, params: _*)
    restClient
      .postBodyless[SpotOrderCreateResponse](
        uri = uri,
        headers = Map("X-MBX-APIKEY" -> config.apiKey)
      )
  }

  override def cancelOrder(
      orderCancel: SpotOrderCancelParams,
      timestamp: Instant
  ): FutureResponse[SpotOrderCancelResponse] = {
    val uri = mkSignedUri(uri = config.orderUri, timestamp = timestamp, orderCancel.toQueryParams: _*)
    restClient
      .delete[SpotOrderCancelResponse](
        uri = uri,
        headers = Map("X-MBX-APIKEY" -> config.apiKey)
      )
  }

  override def cancelAllOrders(
      orderCancel: SpotOrderCancelAllParams,
      timestamp: Instant
  ): FutureResponse[Seq[SpotOrderCancelResponse]] = {
    val uri = mkSignedUri(uri = config.openOrdersUri, timestamp = timestamp, orderCancel.toQueryParams: _*)
    restClient
      .delete[Seq[SpotOrderCancelResponse]](
        uri = uri,
        headers = Map("X-MBX-APIKEY" -> config.apiKey)
      )
  }

//  def tradeStreams(
//      symbol: String,
//      handle: S#Pipe[WebSocketFrame.Data[_], WebSocketFrame]
//  ): F[Response[Either[String, Unit]]] = {
//    val uri = uri"${config.wsBaseUrl}/ws/${symbol.toLowerCase}@trade"
//    restClient.ws[TradeStream](uri, handle)
//  }
//
//  /** The Kline/Candlestick Stream push updates to the current klines/candlestick every second.
//    *
//    * @param symbol
//    *   the symbol
//    * @param interval
//    *   the interval
//    * @return
//    *   a stream of klines
//    */
//  def kLineStreams(
//      symbol: String,
//      interval: Interval,
//      handle: S#Pipe[WebSocketFrame.Data[_], WebSocketFrame]
//  ): F[Response[Either[String, Unit]]] = {
//    val uri = uri"${config.wsBaseUrl}/ws/${symbol.toLowerCase}@kline_${interval.toString}"
//    restClient.ws[TradeStream](uri, handle)
//  }
//
//  /** Order book price and quantity depth updates used to locally manage an order book.
//    *
//    * @param symbol
//    *   the symbol
//    * @return
//    *   a stream of order book price and quantity depth updates
//    */
//  def diffDepthStream(
//      symbol: String,
//      handle: S#Pipe[WebSocketFrame.Data[_], WebSocketFrame]
//  ): F[Response[Either[String, Unit]]] = {
//    val uri = uri"${config.wsBaseUrl}/ws/${symbol.toLowerCase}@depth"
//    restClient.ws[DiffDepthStream](uri, handle)
//  }
//
//  /** Top bids and asks
//    *
//    * @param symbol
//    *   the symbol
//    * @param level
//    *   the level
//    * @return
//    *   a stream of top bids and asks
//    */
//  def partialBookDepthStream(
//      symbol: String,
//      level: Level,
//      handle: S#Pipe[WebSocketFrame.Data[_], WebSocketFrame]
//  ): F[Response[Either[String, Unit]]] = {
//    val uri = uri"${config.wsBaseUrl}/ws/${symbol.toLowerCase}@depth${level.toString}"
//    restClient.ws[PartialDepthStream](uri, handle)
//  }
//
//  /** Pushes any update to the best bid or ask's price or quantity in real-time for all symbols.
//    *
//    * @return
//    *   a stream of best bid or ask's price or quantity for all symbols
//    */
//  def allBookTickersStream(
//      handle: S#Pipe[WebSocketFrame.Data[_], WebSocketFrame]
//  ): F[Response[Either[String, Unit]]] = {
//    val uri = uri"${config.wsBaseUrl}/ws/!bookTicker"
//    restClient.ws[BookTicker](uri, handle)
//  }
}
