package plato.eudemonia.binance.client.spot.response

import io.circe.Decoder
import io.circe.generic.extras.Configuration

object FilterCodecs {
  implicit val genDevConfig: Configuration = Configuration.default.withDiscriminator("filterType")
  import io.circe.generic.extras.semiauto._

  implicit val decoder: Decoder[Filter] = deriveConfiguredDecoder[Filter]
}
