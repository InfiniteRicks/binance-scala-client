package plato.eudemonia.binance.client.spot.response

import io.circe.Decoder

sealed trait Filter

object Filter {
  implicit val decoder: Decoder[Filter] = FilterCodecs.decoder

  case class PRICE_FILTER(minPrice: BigDecimal, maxPrice: BigDecimal, tickSize: BigDecimal)         extends Filter
  case class PERCENT_PRICE(multiplierUp: BigDecimal, multiplierDown: BigDecimal, avgPriceMins: Int) extends Filter
  case class LOT_SIZE(minQty: BigDecimal, maxQty: BigDecimal, stepSize: BigDecimal)                 extends Filter
  case class MARKET_LOT_SIZE(minQty: BigDecimal, maxQty: BigDecimal, stepSize: BigDecimal)          extends Filter
  case class MAX_NUM_ORDERS(maxNumOrders: Int)                                                      extends Filter
  case class MAX_NUM_ALGO_ORDERS(maxNumAlgoOrders: Int)                                             extends Filter
  case class MAX_NUM_ICEBERG_ORDERS(maxNumIcebergOrders: Int)                                       extends Filter
  case class MIN_NOTIONAL(minNotional: BigDecimal, applyToMarket: Boolean, avgPriceMins: Int)       extends Filter
  case class ICEBERG_PARTS(limit: Int)                                                              extends Filter
  case class MAX_POSITION(maxPosition: BigDecimal)                                                  extends Filter
  case class EXCHANGE_MAX_NUM_ORDERS(maxNumOrders: Int)                                             extends Filter
  case class EXCHANGE_MAX_ALGO_ORDERS(maxNumAlgoOrders: Int)                                        extends Filter

}
