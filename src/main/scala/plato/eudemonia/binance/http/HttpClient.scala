package plato.eudemonia.binance.http

import akka.actor.ClassicActorSystemProvider
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{Message, WebSocketRequest, WebSocketUpgradeResponse}
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.Materializer
import akka.stream.scaladsl.Flow

import scala.concurrent.{ExecutionContext, Future}

trait HttpClient {
  def singleRequest(request: HttpRequest): Future[HttpResponse]
  def singleWebSocketRequest[T](request: WebSocketRequest, clientFlow: Flow[Message, Message, T]): (Future[WebSocketUpgradeResponse], T)
}

class HttpClientImpl(implicit system: ClassicActorSystemProvider) extends HttpClient {
  implicit private val ec: ExecutionContext = system.classicSystem.dispatcher
  implicit private val mat: Materializer    = Materializer(system)

  override def singleRequest(request: HttpRequest): Future[HttpResponse] =
    Http().singleRequest(request: HttpRequest): Future[HttpResponse]

  override def singleWebSocketRequest[T](request: WebSocketRequest, clientFlow: Flow[Message, Message, T]): (Future[WebSocketUpgradeResponse], T) =
    Http().singleWebSocketRequest(request, clientFlow)
}
