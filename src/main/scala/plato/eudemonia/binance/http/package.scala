/*
 * Copyright (c) 2021 Paolo Boni
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package plato.eudemonia.binance

import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.Query
import plato.eudemonia.binance.client.exception.BinanceException
import plato.eudemonia.binance.client.exception.BinanceException

import scala.concurrent.Future


package object http {

  type BType[T] = Either[BinanceException, T]
  type FutureResponse[T] = Future[BType[T]]

  implicit class UriOps(val uri: Uri) extends AnyVal {
    def queryStr: String = uri.toString().span(_ != '?')._2.tail
  }

  implicit class QueryOps(val query: Query) extends AnyVal {
    def append(key: String, value: String): Query = Query(query :+ (key -> value): _*)
    def append(addends: Map[String, String]): Query = Query(query :++ addends: _*)
    def append(addends: (String, String)*): Query = Query(query :++ addends: _*)
  }
}
