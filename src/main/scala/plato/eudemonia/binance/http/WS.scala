package plato.eudemonia.binance.http

import akka.http.scaladsl.model.ws.{Message, WebSocketRequest, WebSocketUpgradeResponse}
import akka.stream.Materializer
import akka.stream.scaladsl.Flow

import scala.concurrent.Future

trait WS {

  def doStream[T](
      request: WebSocketRequest,
      clientFlow: Flow[Message, Message, T]
  )(implicit mat: Materializer): (Future[WebSocketUpgradeResponse], T)

}

class WSImpl(
    http: HttpClient
) extends WS {

  def doStream[T](
      request: WebSocketRequest,
      clientFlow: Flow[Message, Message, T]
  )(implicit mat: Materializer): (Future[WebSocketUpgradeResponse], T) =
    http.singleWebSocketRequest(request, clientFlow)

}