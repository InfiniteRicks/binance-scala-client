package plato.eudemonia.binance.http

import akka.http.scaladsl.marshalling.{EmptyValue, Marshal, ToEntityMarshaller}
import akka.http.scaladsl.model.HttpHeader.ParsingResult
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshal}
import akka.stream.Materializer
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import plato.eudemonia.binance.client.exception.BinanceException

import scala.concurrent.{ExecutionContext, Future}

trait REST {

  def get[T: FromEntityUnmarshaller](
      uri: Uri,
      headers: Map[String, String] = Map.empty
  ): FutureResponse[T]

  def post[Body: ToEntityMarshaller, T: FromEntityUnmarshaller](
      uri: Uri,
      requestBody: Body,
      headers: Map[String, String] = Map.empty
  ): FutureResponse[T]

  def postBodyless[T: FromEntityUnmarshaller](
      uri: Uri,
      headers: Map[String, String] = Map.empty
  ): FutureResponse[T]

  def delete[T: FromEntityUnmarshaller](
      uri: Uri,
      headers: Map[String, String] = Map.empty
  ): FutureResponse[T]

  def doRequest(
      request: HttpRequest
  ): Future[HttpResponse]
}

class RESTImpl(
    http: HttpClient
)(implicit ec: ExecutionContext, mat: Materializer)
    extends REST {

  override def get[T: FromEntityUnmarshaller](uri: Uri, headers: Map[String, String]): FutureResponse[T] = {

    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = uri,
      headers = parseHeader(headers)
    )
    for {
      response <- doRequest(request)
      result   <- unmarshal[T](response)
    } yield result
  }

  override def post[Body: ToEntityMarshaller, T: FromEntityUnmarshaller](
      uri: Uri,
      requestBody: Body,
      headers: Map[String, String]
  ): FutureResponse[T] = {
    for {
      entity <- Marshal(requestBody).to[RequestEntity]
      request = HttpRequest(
        method = HttpMethods.POST,
        uri = uri,
        headers = parseHeader(headers),
        entity = entity
      )
      response <- doRequest(request)
      result   <- unmarshal[T](response)
    } yield result
  }

  override def postBodyless[T: FromEntityUnmarshaller](
      uri: Uri,
      headers: Map[String, String]
  ): FutureResponse[T] = {

    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = uri,
      headers = parseHeader(headers)
    )
    for {
      response <- doRequest(request)
      result   <- unmarshal[T](response)
    } yield result
  }

  override def delete[T: FromEntityUnmarshaller](uri: Uri, headers: Map[String, String]): FutureResponse[T] = {

    val request = HttpRequest(
      method = HttpMethods.DELETE,
      uri = uri,
      headers = parseHeader(headers)
    )
    for {
      response <- doRequest(request)
      result   <- unmarshal[T](response)
    } yield result
  }

  override def doRequest(request: HttpRequest): Future[HttpResponse] = http.singleRequest(request)

  private def parseHeader(headers: Map[String, String]): Seq[HttpHeader] = {
    val parsed = headers.map { case (k, v) => HttpHeader.parse(k, v) }.toSeq
    val valid  = parsed.collect { case ParsingResult.Ok(header, _) => header }
    valid
  }

  private def unmarshal[T: FromEntityUnmarshaller](response: HttpResponse): FutureResponse[T] =
    response.status match {
      case StatusCodes.OK => Unmarshal(response.entity).to[T].map(Right(_))
      case _              => Unmarshal(response.entity).to[BinanceException].map(Left(_))
    }
}
