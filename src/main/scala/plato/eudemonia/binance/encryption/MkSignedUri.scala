/*
 * Copyright (c) 2021 Paolo Boni
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package plato.eudemonia.binance.encryption

import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.Query
import plato.eudemonia.binance.client.common.parameters.TimeParams
import plato.eudemonia.binance.http.QueryParamsConverter._
import plato.eudemonia.binance.http.UriOps
import plato.eudemonia.binance.http.QueryOps

import java.time.Instant

class MkSignedUri(recvWindow: Int, apiSecret: String) {

  def apply(uri: Uri, timestamp: Instant, params: (String, String)*): Uri = {
    val paramsToQuery      = Query(params: _*)
    val timeParams         = TimeParams(timestamp.toEpochMilli, recvWindow).toQueryParams.toList
    val query              = paramsToQuery.append(timeParams: _*)
    val uriAndParams       = uri.withQuery(query)
    val signature          = HMAC.sha256(apiSecret, uriAndParams.queryStr)
    val queryWithSignature = query.append("signature", signature)
    uri.withQuery(queryWithSignature)
  }
}
