# Scala client for Binance API
<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-2-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->

[![Build Status](https://github.com/paoloboni/binance-scala-client/actions/workflows/ci.yml/badge.svg)](https://github.com/paoloboni/binance-scala-client/actions?query=workflow)
[![Latest version](https://img.shields.io/maven-central/v/plato.eudemonia.binance/binance-scala-client_2.13.svg)](https://search.maven.org/artifact/plato.eudemonia.binance/binance-scala-client_2.13)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 
[![Join the chat at https://gitter.im/binance-scala-client/community](https://badges.gitter.im/binance-scala-client/community.svg)](https://gitter.im/binance-scala-client/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Join the chat on Discord at https://discord.gg/7KrBehYs55](https://img.shields.io/discord/878591852331290634?logo=discord)](https://discord.gg/7KrBehYs55)
[![Support](https://img.shields.io/badge/support-buymeacoffee-green)](https://www.buymeacoffee.com/paoloboni)

A functional Scala client for Binance, powered by [cats-effect](https://typelevel.org/cats-effect/) and [fs2](https://fs2.io/).

This client is rate limited, based on [Binance API specification](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md).

## Getting started

If you use sbt add the following dependency to your build file:

```sbtshell
libraryDependencies += "plato.eudemonia.binance" %% "binance-scala-client" % "<version>"
```

## APIs supported

### Spot API

#### Rest
* [Exchange information](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#exchange-information): Current exchange trading rules and symbol information
* [Kline/Candlestick data](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#klinecandlestick-data): Kline/candlestick bars for a symbol
* [Symbol price ticker](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#symbol-price-ticker): Latest price for a symbol or symbols
* [Balance information](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#account-information-user_data): Current balance information
* [New order (trade)](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#new-order--trade): Send in a new order
* [Cancel order (trade)](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#cancel-order-trade): Cancel an active order
* [Cancel open orders (trade)](https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#cancel-all-open-orders-on-a-symbol-trade): Cancels all active orders on a symbol
* [Order book](https://binance-docs.github.io/apidocs/spot/en/#order-book): Order book depth on a symbol

#### WebSocket
* [Kline/Candlestick Streams](https://binance-docs.github.io/apidocs/spot/en/#kline-candlestick-streams): The Kline/Candlestick Stream push updates to the current klines/candlestick every second.
* [Diff. Depth Stream](https://binance-docs.github.io/apidocs/spot/en/#diff-depth-stream): Order book price and quantity depth updates used to locally manage an order book.
* [All Book Tickers Stream](https://binance-docs.github.io/apidocs/spot/en/#all-book-tickers-stream): Pushes any update to the best bid or ask's price or quantity in real-time for all symbols.
* [Partial Book Depth Streams](https://binance-docs.github.io/apidocs/spot/en/#partial-book-depth-streams): Top bids and asks.
* [Trade Streams](https://binance-docs.github.io/apidocs/spot/en/#trade-streams): The Trade Streams push raw trade information.
* [Aggregate Trade Streams](https://binance-docs.github.io/apidocs/spot/en/#aggregate-trade-streams): The Aggregate Trade Streams push trade information that is aggregated for a single taker order.


## Initialise the client

The client initialisation returns an API object as a [Cats Resource](https://typelevel.org/cats-effect/docs/std/resource).
There are two factories available at the moment, one for Spot API and the other one for Future API.

### Spot client

```scala
val codeHere = ""
```

## Documentation

The API documentation is available [here](https://paoloboni.github.io/binance-scala-client/latest/api/).

## Example app

This is a sample app to monitor the exchange prices (fetch every 5 seconds).

```scala
val codeHere = ""
```

## Contributing

### How to run unit and integration tests

```
sbt test
```

### How to run end-to-end tests

```
FAPI_API_KEY=<your-fapi-api-key> \
    FAPI_SECRET_KEY=<your-fapi-secret-key> \
    SPOT_API_KEY=<your-spot-api-key> \
    SPOT_SECRET_KEY=<your-spot-secret-key> \
    sbt e2e:test
```

### How to get Spot API and secret keys

- Navigate to https://testnet.binance.vision
- Login with your Github account
- Click on "Generate HMAC_SHA256 Key"
- Enter a description and press "Generate"
- Take note of the generated values

### How to get Future API and secret keys

- Navigate to https://testnet.binancefuture.com
- Register a new account or login with an existing one
- Click on "API key" link in the lower part of the screen
- Take note of the values

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td align="center"><a href="https://github.com/DarkWingMcQuack"><img src="https://avatars.githubusercontent.com/u/38857302?v=4?s=100" width="100px;" alt=""/><br /><sub><b>DarkWingMcQuack</b></sub></a><br /><a href="https://github.com/paoloboni/binance-scala-client/commits?author=DarkWingMcQuack" title="Code">💻</a></td>
    <td align="center"><a href="https://github.com/Swoorup"><img src="https://avatars.githubusercontent.com/u/3408009?v=4?s=100" width="100px;" alt=""/><br /><sub><b>Swoorup Joshi</b></sub></a><br /><a href="https://github.com/paoloboni/binance-scala-client/commits?author=Swoorup" title="Code">💻</a></td>
  </tr>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!
