import Dependencies._

name := "binance-akka-client"

organization := "plato.eudemonia"

version := "0.0.1"

scalaVersion := "2.13.7"

libraryDependencies ++=
  akkaHttp ++ akka ++ circe ++ enumeratum ++ logback ++ tests ++ ficus

versionScheme := Some("early-semver")

useCoursier := false

scalacOptions ++= Seq(
  "-Ymacro-annotations",
  "-language:implicitConversions",
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps",
  "-encoding", "utf8"
)