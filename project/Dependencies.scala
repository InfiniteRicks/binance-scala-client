import sbt._
import sbt.librarymanagement.ModuleID

object Dependencies {

  private val enumeratumVersion: String = "1.7.0"
  private val scalatestVersion: String = "3.2.10"
  private val scalacheckVersion: String = "1.15.4"
  private val scalatestplusVersions: (String, String) = (scalatestVersion + ".0", scalacheckVersion.split('.').take(2).mkString("-"))
  private val scalaMockVersion: String = "5.1.0"
  private val akkaHttpVersion: String = "10.2.7"
  private val akkaVersion: String = "2.6.17"
  private val logbackVersion: String = "1.2.7"
  private val ficusVersion: String = "1.5.1"
  private val circeVersion: String = "0.14.1"
  private val circeAkkaHttpVersion = "1.39.2"

  lazy val circe: Seq[ModuleID] = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser",
    "io.circe"     %% "circe-generic-extras"
  ).map(_ % circeVersion)

  val enumeratum: Seq[ModuleID] = Seq(
    "com.beachape" %% "enumeratum" % enumeratumVersion,
    "com.beachape" %% "enumeratum-circe" % enumeratumVersion,
  )

  val scalaTest: Seq[ModuleID] = Seq(
    "org.scalatest" %% "scalatest-flatspec" % scalatestVersion % Test,
    "org.scalatest" %% "scalatest-shouldmatchers" % scalatestVersion % Test
  )

  val scalaCheck: Seq[ModuleID] = Seq(
    // https://mvnrepository.com/artifact/org.scalacheck/scalacheck
    "org.scalacheck" %% "scalacheck" % "1.15.4" % Test,
    "org.scalatestplus" %% s"scalacheck-${scalatestplusVersions._2}" % scalatestplusVersions._1 % "test"
  )

  val scalaMock: Seq[ModuleID] = Seq(
    "org.scalamock" %% "scalamock" % scalaMockVersion % Test
  )

  lazy val akkaHttp: Seq[ModuleID] = Seq(
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "de.heikoseeberger" %% "akka-http-circe" % circeAkkaHttpVersion
  )

  lazy val akka: Seq[ModuleID] = Seq(
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream-typed" % akkaVersion,
    "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  )

  lazy val logback: Seq[ModuleID] = Seq(
    "ch.qos.logback" % "logback-classic" % logbackVersion
  )

  lazy val ficus: Seq[ModuleID] = Seq(
    "com.iheart" %% "ficus" % ficusVersion
  )

  lazy val tests: Seq[ModuleID] = scalaTest ++ scalaCheck ++ scalaMock
}
